﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour {

    public GameObject mole;
    public bool done;
    public float spawnTime;

	// Use this for initialization
	void Start ()
    {
        spawnTime = Random.Range(1f,6f);
        StartCoroutine("Spawn");
	}
	
	// Update is called once per frame
    void Update()
    {
        if (done == true)
        {
            spawnTime = Random.Range(1f,6f);
            StartCoroutine("Spawn");
            done = false;
        }
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(spawnTime);
        GameObject obj = (GameObject)Instantiate(mole, transform.position - new Vector3(0, 0.45f, 0), Quaternion.identity);
        obj.GetComponent<mole>().waitTime = Random.Range(3f,5f);
        obj.GetComponent<mole>().hole = transform.gameObject;
    }
}
